## Tentang Aplikasi
Ini adalah aplikasi Point Of Sales (POS) sederhana pada sebuah prusahaan berbasis website, terdiri dari 3 aktor mulai dari director, sales dan administrator. Untuk membangun aplikasi ini menggunakan framework Laravel dengan bahasa pemrograman PHP7.2++ dan database yang digunakan adalah MySQL

## Teknologi
- PHP 7.2++
- Composer
- MySQL
- WorkBranch / PHPmyadmin
- Browser (Recomended Chrome)
- IDE (Recomended Visual Studio Code)

## Cara Instal
- Buka Terminal / CommentPrompt (CMD) untuk windows
- Arahkan terminal pada folder yang diinginkan untuk menyimpan file project, contoh, cd Document/ (enter), saat ini kamu ada difolder Document, (jika masih kurang paham tentang terminal / Commentpromt kamu bisa baca-baca manualnya)
- Ketik perintah berikut untuk clone / download file project : "https://gitlab.com/renofinsa/skripsi-yeni.git"
- Buka Phpmyadmin dan buatlah database, misalnya : pos_database
- Buka File Project menggunakan IDE 
- Cari file .env.example, lalu diduplikat (copy paste) menjadi .env
- Setup file .env, pada baris 12 (database), 13(username), 14(password), silakan kamu setting sesuai settingan kamu, jika kamu menggunakan mysql biasanya tidak ada password jadi kamu bisa kosongkan sedangkan username mysql adalah root untuk default, karena kamu sudah buat database di phpmyadmin dengan nama pos_database, kamu bisa ketik nama tersebut dibagian database, lalu disimpan
- ketika perintah "composer install"
- ketika perintah "php artisan key:generate"
- Buka Terminal lagi, lalu ketik perintah berikut, "php artisan storage:link" untuk sync file dibagian folder public
- Ketik perintah "php artisan migrate", untuk membuat table database
- Ketik printah "php artisan db:seed", untuk membuat field user agar bisa akse aplikasi
- Ketik perintah "php artisan serv", untuk menjalankan project
- lalu kamu bisa mengakses project dengan link "localhost::8000"
- pastikan port yang digunakan agar tidak terjadi kesalahan, biasanya port yang digunakan bisa berbeda.
- Dan kamu bisa akses aplikasi dengan menggunakan akses berikut:

## Akses Project
- Director : director@pos.com
- Accounting : accounting@pos.com
- Admin Sales : adminsales@pos.com
seluruh aktor menggunakan password yang sama
    - Password : admin
