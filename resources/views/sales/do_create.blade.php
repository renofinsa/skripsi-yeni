@extends('layouts.app-thrid')
@section('who','Admin Sales')
@section('navbar')
    @include('sales.navbar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sales')}}">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{route('sales-do')}}">Delivery Order</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah</li>
        </ol>
    </nav>

    <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tambah Produk</h6>
            </div>
            <div class="card-body">
              <form action="{{route('sales-do-store')}}" method="post">
                  @csrf
                  <div class="form-group">
                      <label for="">Id Pelanggan</label>
                      <select name="customer_id" id="customer_id" class="form-control @error('customer_id') is-invalid @enderror" name="customer_id" required>
                          @foreach ($pelanggan as $i)
                              <option value="{{$i->id}}">{{$i->nama}} - {{$i->id}}</option>
                          @endforeach
                      </select>
                        @error('customer_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  </div>
                  <div class="form-group">
                      <label for="">Diskon</label>
                      <input type="number" min="0" max="99" class="form-control @error('diskon') is-invalid @enderror" name="diskon" value="{{ old('diskon') }}" required autocomplete="diskon" placeholder="20">
                        @error('diskon')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  </div>
                  <div class="form-group">
                      <label for="">Tanggal Jatuh Tempo</label>
                      <input type="date" min="{{date("Y-m-d")}}" class="form-control @error('tanggal_jatuh_tempo') is-invalid @enderror" name="tanggal_jatuh_tempo" value="{{ old('tanggal_jatuh_tempo') }}" required autocomplete="tanggal_jatuh_tempo">
                        @error('tanggal_jatuh_tempo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  </div>
                  <div class="form-group">
                      <button type="submit" name="submit" class="btn btn-primary form-control">Lanjut</button>
                  </div>
              </form>
            </div>
          </div>

@endsection
