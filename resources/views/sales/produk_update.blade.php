@extends('layouts.app-thrid')
@section('who','Admin Sales')
@section('navbar')
    @include('sales.navbar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sales')}}">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{route('sales-produk')}}">Produk</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ubah</li>
        </ol>
    </nav>

    <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Ubah Produk</h6>
            </div>
            <div class="card-body">
              <form action="{{route('sales-produk-update', $data->id)}}" method="post">
                  @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="form-group">
                      <label for="">Id Produk</label>
                      <input type="text" class="form-control @error('id_produk') is-invalid @enderror" name="id_produk" value="{{$data->idproduk }}" required autocomplete="id_produk" placeholder="AA01">
                        @error('id_produk')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  </div>
                  <div class="form-group">
                      <label for="">Nama Produk</label>
                      <input type="text" class="form-control @error('nama_produk') is-invalid @enderror" name="nama_produk" value="{{ $data->nama_produk }}" required autocomplete="nama_produk" placeholder="Nama Produk">
                        @error('nama_produk')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  </div>
                  <div class="form-group">
                      <label for="">Harga Produk /Unit (IDR)</label>
                      <input type="number" min="0" class="form-control @error('harga') is-invalid @enderror" name="harga" value="{{ $data->harga }}" required autocomplete="harga" placeholder="5000">
                        @error('harga')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  </div>
                  <div class="form-group">
                      <button type="submit" name="submit" class="btn btn-primary form-control">Ubah</button>
                  </div>
              </form>
            </div>
          </div>

@endsection
