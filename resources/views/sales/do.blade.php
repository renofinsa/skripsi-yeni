@extends('layouts.app-second')
@section('who','Admin Sales')
@section('navbar')
    @include('sales.navbar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sales')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Delivery Order</li>
        </ol>
    </nav>

    <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">
                        {{-- <a href="#" class="btn btn-dark" style="float:left"><i class="fas fa-fw fa-download"></i> Unduh</a> --}}
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('sales-do-create')}}" class="btn btn-primary" style="float:right"><i class="fas fa-fw fa-plus"></i> Tambah</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>No. Invoice</th>
                      <th>Pelanggan</th>
                      <th>Diskon</th>
                      <th>Tanggal Pembuatan</th>
                      <th>Tanggal Jatuh Tempo</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>No. Invoice</th>
                      <th>Pelanggan</th>
                      <th>Diskon</th>
                      <th>Tanggal Pembuatan</th>
                      <th>Tanggal Jatuh Tempo</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php $no = 0 ?>
                    @foreach ($invoice as $i)
                    <?php $no++ ?>
                    <tr>
                      <td>{{$no}}</td>
                      <td>#{{$i->invoice_number}}</td>
                      <td>{{$i->pelanggan->nama}}</td>
                      <td>{{$i->diskon}}%</td>
                      <td> {{date('d M Y', strtotime($i->tanggal_pembuatan))}}</td>
                      <td> {{date('d M Y', strtotime($i->tanggal_jatuh_tempo))}}</td>
                      <td>
                        <a href="/sales/do/print={{$i->id}}" target="_blank" class="btn btn-danger"><i class="fas fa-print"></i> Print</a>
                        <a href="/sales/do/add={{$i->id}}" class="btn btn-success"><i class="fas fa-pencil-alt"></i> Ubah</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

@endsection
