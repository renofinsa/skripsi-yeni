@extends('layouts.app-fourth')
@section('who','Admin Sales')
@section('navbar')
    @include('sales.navbar')
@endsection

@section('content')
    <!-- Page Heading -->

    <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="m-0 font-weight-bold text-primary">No. Invoice: #{{$invoice->invoice_number}}</h6>
                        <h6 class="m-0 font-weight-bold text-primary">Diskon : {{$invoice->diskon}}%</h6>
                        <h6 class="m-0 font-weight-bold text-primary">Tanggal Jatuh Tempo : {{date('d M Y', strtotime($invoice->tanggal_jatuh_tempo))}}</h6>
                    </div>
                    <div class="col-md-6">
                        <button style="float:right" type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">
                            <i class="fas fa-fw fa-plus"></i> Tambah
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Id Produk</th>
                      <th>Nama Produk</th>
                      <th>Qty</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>#</th>
                      <th>Id Produk</th>
                      <th>Nama Produk</th>
                      <th>Qty</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      <?php $no = 0 ?>
                      @foreach ($detailproduct as $i)
                      <?php $no++ ?>
                    <tr>
                      <td style="width:30px; text-align:center">{{$no}}</td>
                      <td>{{$i->produk->idproduk}}</td>
                      <td>{{$i->produk->nama_produk}}</td>
                      <td>{{$i->qty}}</td>
                      <td style="width: 50px">
                            <button style="float:right" type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$i->id}}">
                                    <i class="fas fa-fw fa-trash"></i>
                                </button>
                      </td>
                    </tr>


                    <!-- Modal Hapus Produk -->
                    <div class="modal fade" id="delete{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Produk</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{route('sales-do-addproduct-delete',$i->id)}}" method="post">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input name="penjualans_id" type="hidden" value="{{$invoice->id}}">
                                    <div class="modal-body">
                                        <p>Apa kamu yakin ingin menghapus produk ini dari invoice : {{$invoice->id}}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                                        <button type="submit" class="btn btn-primary">Hapus</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    @endforeach
                  </tbody>
                </table>
              </div>
              <div class="form-group">
                  <a href="{{route('sales-do')}}" class="btn btn-primary form-control">Tutup</a>
              </div>
            </div>
          </div>



            <!-- Modal Tambah Produk -->
            <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Produk</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{route('sales-do-addproduct-store')}}" method="post">
                            @csrf
                            <input name="penjualans_id" type="hidden" value="{{$invoice->id}}">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="">Produk</label>
                                    <select name="produks_id" class="form-control" required>
                                        @foreach ($product as $i)
                                            <option value="{{$i->id}}">{{$i->idproduk}} - {{$i->nama_produk}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="">Qty</label>
                                        <input name="qty" type="number" min="1" max="10000" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


@endsection
