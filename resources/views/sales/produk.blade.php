@extends('layouts.app-second')
@section('who','Admin Sales')
@section('navbar')
    @include('sales.navbar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sales')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Produk</li>
        </ol>
    </nav>

    <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{route('sales-produk-download')}}" target="_blank" class="btn btn-dark" style="float:left"><i class="fas fa-fw fa-download"></i> Unduh</a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('sales-produk-create')}}" class="btn btn-primary" style="float:right"><i class="fas fa-fw fa-plus"></i> Tambah</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Id Produk</th>
                      <th>Nama Produk</th>
                      <th>Harga</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>#</th>
                      <th>Id Produk</th>
                      <th>Nama Produk</th>
                      <th>Harga</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      <?php $no = 0 ?>
                      @foreach ($data as $i)
                      <?php $no++ ?>
                    <tr>
                      <td style="width:30px; text-align:center">{{$no}}</td>
                      <td>{{$i->idproduk}}</td>
                      <td>{{$i->nama_produk}}</td>
                      <td>Rp {{number_format($i->harga,0,',','.')}}</td>
                      <td style="width: 100px">
                          <a href="{{route('sales-produk-update',$i->id)}}" class="btn btn-success"><i class="fas fa-fw fa-pen"></i> Ubah</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

@endsection
