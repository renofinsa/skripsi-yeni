<!-- Nav Item - Dashboard -->
      <li class="nav-item {{ (request()->is('sales')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('sales')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Beranda</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Fitur
      </div>

      <li class="nav-item {{ (request()->is('sales/do')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('sales-do')}}">
          <i class="fas fa-fw fa-file"></i>
          <span>Delivery Order</span></a>
      </li>
      <li class="nav-item {{ (request()->is('sales/delivery')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('sales-delivery')}}">
          <i class="fas fa-fw fa-paper-plane"></i>
          <span>Kelola Pengiriman</span></a>
      </li>
      <li class="nav-item {{ (request()->is('sales/pelanggan')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('sales-pelanggan')}}">
          <i class="fas fa-fw fa-users"></i>
          <span>Pelanggan</span></a>
      </li>
      <li class="nav-item {{ (request()->is('sales/produk')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('sales-produk')}}">
          <i class="fas fa-fw fa-shopping-basket"></i>
          <span>Produk</span></a>
      </li>
