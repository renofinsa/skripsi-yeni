@extends('layouts.app-second')
@section('who','Admin Sales')
@section('navbar')
    @include('sales.navbar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sales')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pengiriman</li>
        </ol>
    </nav>

    <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">
                        {{-- <a href="#" class="btn btn-dark" style="float:left"><i class="fas fa-fw fa-download"></i> Unduh</a> --}}
                    </div>
                    <div class="col-md-6">
                        {{-- <a href="{{route('sales-do-create')}}" class="btn btn-primary" style="float:right"><i class="fas fa-fw fa-plus"></i> Tambah</a> --}}
                    </div>
                </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>No. Invoice</th>
                      <th>Pelanggan</th>
                      <th>Diskon</th>
                      <th>Tanggal Pembuatan</th>
                      <th>Tanggal Jatuh Tempo</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>No. Invoice</th>
                      <th>Pelanggan</th>
                      <th>Diskon</th>
                      <th>Tanggal Pembuatan</th>
                      <th>Tanggal Jatuh Tempo</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php $no = 0 ?>
                    @foreach ($invoice as $i)
                    <?php $no++ ?>
                    <tr>
                      <td>{{$no}}</td>
                      <td>#{{$i->invoice_number}}</td>
                      <td>{{$i->pelanggan->nama}}</td>
                      <td>{{$i->diskon}}%</td>
                      <td> {{date('d M Y', strtotime($i->tanggal_pembuatan))}}</td>
                      <td> {{date('d M Y', strtotime($i->tanggal_jatuh_tempo))}}</td>
                      <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#send{{$i->id}}">
                            <i class="far fa-paper-plane"></i> Kirim
                        </button>
                      </td>
                    </tr>

                    <!-- Konfirmasi Pengiriman -->
                    <div class="modal fade" id="send{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form action="{{route('sales-delivery-update', $i->id)}}" method="post">
                                @csrf
                                <input type="hidden" name="_method" value="PATCH">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pengriman</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Benar kamu ingin melakukan pengiriman pada tanggal {{date('d-M-Y')}} untuk nomer invoice : #{{$i->invoice_number}} ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Urungkan</button>
                                    <button type="submit" class="btn btn-primary">Iya</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>





                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>






@endsection
