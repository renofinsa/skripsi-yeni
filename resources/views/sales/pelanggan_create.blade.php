@extends('layouts.app-thrid')
@section('who','Admin Sales')
@section('navbar')
    @include('sales.navbar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sales')}}">Beranda</a></li>
            <li class="breadcrumb-item"><a href="{{route('sales-pelanggan')}}">Pelanggan</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah</li>
        </ol>
    </nav>

    <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tambah Pelanggan</h6>
            </div>
            <div class="card-body">
              <form action="{{route('sales-pelanggan-store')}}" method="post">
                  @csrf
                  <div class="form-group">
                      <label for="">Nama</label>
                      <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required autocomplete="nama" placeholder="nama">
                        @error('nama')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  </div>
                  <div class="form-group">
                      <label for="">Email</label>
                      <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="nama@doamin.com">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  </div>
                  <div class="form-group">
                      <label for="">Telepon</label>
                      <input type="text" min="0" class="form-control @error('telepon') is-invalid @enderror" name="telepon" value="{{ old('telepon') }}" required autocomplete="telepon" placeholder="082212341234">
                        @error('telepon')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  </div>
                  <div class="form-group">
                      <label for="">Alamat</label>
                      <textarea name="alamat" class="form-control @error('alamat') is-invalid @enderror" id="" cols="30" rows="5" placeholder="alamat">{{ old('alamat') }}</textarea>
                        @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                  </div>
                  <div class="form-group">
                      <button type="submit" name="submit" class="btn btn-primary form-control">Tambah</button>
                  </div>
              </form>
            </div>
          </div>

@endsection
