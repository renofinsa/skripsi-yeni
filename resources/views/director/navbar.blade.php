      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="/">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Beranda</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Fitur
      </div>

       <li class="nav-item {{ (request()->is('director/invoice')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('director-invoice')}}">
          <i class="fas fa-file-invoice"></i>
          <span>Penjualan</span></a>
      </li>
      <li class="nav-item {{ (request()->is('director/debit')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('director-debit')}}">
          <i class="fas fa-file-invoice"></i>
          <span>Piutang</span></a>
      </li>
      <li class="nav-item {{ (request()->is('director/payment')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('director-payment')}}">
          <i class="fas fa-file-invoice-dollar"></i>
          <span>Pembayaran</span></a>
      </li>
