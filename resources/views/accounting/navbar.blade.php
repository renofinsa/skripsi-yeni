      <!-- Nav Item - Dashboard -->
      <li class="nav-item {{ (request()->is('accounting')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('accounting')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Beranda</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Fitur
      </div>

      <li class="nav-item {{ (request()->is('accounting/invoice')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('accounting-invoice')}}">
          <i class="fas fa-file-invoice"></i>
          <span>Penjualan</span></a>
      </li>
      <li class="nav-item {{ (request()->is('accounting/debit')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('accounting-debit')}}">
          <i class="fas fa-file-invoice"></i>
          <span>Piutang</span></a>
      </li>
      <li class="nav-item {{ (request()->is('accounting/payment')) ? 'active' : '' }}">
        <a class="nav-link" href="{{route('accounting-payment')}}">
          <i class="fas fa-file-invoice-dollar"></i>
          <span>Pembayaran</span></a>
      </li>
