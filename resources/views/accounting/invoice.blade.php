@extends('layouts.app-second')
@section('who','Accounting')
@section('title','Penjualan')
@section('navbar')
    @include('accounting.navbar')
@endsection

@section('content')
    <!-- Page Heading -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('accounting')}}">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Penjualan</li>
        </ol>
    </nav>

    <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-dark" style="float:left" data-toggle="modal" data-target="#laporan">
                            <i class="fas fa-fw fa-download"></i> Unduh
                        </button>
                    </div>
                    <div class="col-md-6">
                        {{-- <a href="{{route('sales-do-create')}}" class="btn btn-primary" style="float:right"><i class="fas fa-fw fa-plus"></i> Tambah</a> --}}
                    </div>
                </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>No. Invoice</th>
                      <th>Pelanggan</th>
                      <th>Tanggal Pembuatan</th>
                      <th>Tanggal Jatuh Tempo</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>No. Invoice</th>
                      <th>Pelanggan</th>
                      <th>Tanggal Pembuatan</th>
                      <th>Tanggal Jatuh Tempo</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php $no = 0 ?>
                    @foreach ($invoice as $i)
                    <?php $no++ ?>
                    <tr>
                        <td>{{$no}}</td>
                        <td>#{{$i->invoice_number}}</td>
                        <td>{{$i->pelanggan->nama}}</td>
                        <td> {{date('d M Y', strtotime($i->tanggal_pembuatan))}}</td>
                        <td> {{date('d M Y', strtotime($i->tanggal_jatuh_tempo))}}</td>
                        <td style="text-transform: capitalize">{{$i->status}}</td>
                        <td>
                            <a href="/accounting/invoice/print={{$i->id}}" target="_blank" class="btn btn-danger"><i class="fas fa-print"></i> Print</a>
                            {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#payment{{$i->id}}">
                                <i class="fas fa-coins"></i> Bayar
                            </button> --}}
                        </td>
                    </tr>


                    <!-- Modal Pembayaran -->
                    <div class="modal fade" id="payment{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pembayaran</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="{{route('accounting-payment-do',$i->id)}}" method="post">
                                    <div class="form-group">
                                        <label for="">Tanggal Pelunasan</label>
                                        <input type="date" class="form-control" name="tanggal_pelunasan">
                                    </div>
                                {{-- Invoice No {{$i->id}} akan melakukan pembayaran? Sudahkah kamu mengecek kembali ? --}}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Urungkan</button>
                                    @csrf
                                    <input type="hidden" name="_method" value="PATCH">
                                    <button type="submit" class="btn btn-primary">Ya Benar, Konfirmasi Sekarang</button>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>



                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>


          <!-- Modal Laporan Piutang -->
            <div class="modal fade" id="laporan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <form action="{{route('accounting-invoicesemua')}}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Laporan Penjualan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Pelanggan</label>
                                <select name="customer_id" class="form-control">
                                    <option value="0">Semua</option>
                                    @foreach ($customer as $item)
                                    <option value="{{$item->id}}"> {{$item->nama}} </option>

                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Dari tanggal</label>
                                <input name="from" type="date" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Sampai tanggal</label>
                                <input name="to" type="date" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Urungkan</button>

                        <button type="submit" class="btn btn-primary">Terbitkan Laporan</button>
                    </div>
                    </form>
                    </div>
                </div>
            </div>

@endsection
