<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Laporan Pelunasan</title>
  </head>
  <body>
  <img src="img/logo_indomasjaya.png" alt="">
  <hr>
  <h2 style="text-align:center">Laporan Pelunasan</h2>
  <p>
      {{-- No. Invoice : {{$data->id}}<br> --}}
  </p>
  <hr>
  <table>
    <tr>
        <th style="width:150px;text-align:left;padding:5px">No Faktur</th>
        <th style="width:150px;text-align:left;padding:5px">Tanggal Pembuatan</th>
        <th style="width:150px;text-align:left;padding:5px">Jatuh Tempo</th>
        <th style="width:150px;text-align:left;padding:5px">Nilai Faktur</th>
    </tr>
    @foreach ($posts as $i)
    <tr>
        <td style="text-align:left;padding:5px">#{{$i->invoice}}<br>({{$i->nama}})</td>
        <td style="text-align:left;padding:5px">{{date('d M Y', strtotime($i->tanggal_pembuatan))}}</td>
        <td style="text-align:left;padding:5px">{{date('d M Y', strtotime($i->tanggal_jatuh_tempo))}}</td>
        <td style="text-align:left;padding:5px">Rp {{number_format($i->total_harga,0,',','.')}}</td>
    </tr>

    @endforeach
    <hr>
</table>
<h4 style="float:right; margin-right:140px; padding-top: 10px; color:red">Total : Rp {{number_format($total[0]->total_harga,0,',','.')}}</h4>
<h4 style="float:left;position: absolute; margin-top:8px; color:red">{{date('d M Y', strtotime($from))}} - {{date('d M Y', strtotime($to))}} </h4>



  </body>
</html>
