<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Invoice!</title>
  </head>
  <body>
  <img src="img/logo_indomasjaya.png" alt="">
  <hr>
  <h2 style="text-align:center">Nota Pelunasan #{{$invoice->invoice_number}}</h2>
  <p>
      No. Invoice : #{{$invoice->invoice_number}}<br>

      Dikirim ke : <span style="text-transform: uppercase">{{$invoice->pelanggan->nama}}</span> <br>
      Alamat : {{$invoice->pelanggan->alamat}}
  </p>
  <p style="position:absolute; float:right; padding-top: -70px">
        Tanggal Pembuatan : {{date('d M Y', strtotime($invoice->tanggal_pembuatan))}} <br>
        Tanggal Jatuh Tempo : {{date('d M Y', strtotime($invoice->tanggal_jatuh_tempo))}} <br>
        <span style="color:red">Tanggal Pelunasan : {{date('d M Y', strtotime($invoice->tanggal_pelunasan))}} <br></span>
  </p>
  <hr>
  <table>
        <tr>
            <th style="width:50px">No.</th>
            <th style="width:200px">Nama Produk</th>
            <th style="width:150px">Harga</th>
            <th style="width:100px">Qty</th>
            <th style="width:150px">Sub Total</th>
        </tr>
        <?php $no = 0 ?>
        @foreach ($detail as $i)
        <?php $no++ ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$i->produk->nama_produk}}</td>
            <td>Rp {{number_format($i->harga,0,',','.')}}</td>
            <td>{{number_format($i->qty,0,',','.')}}</td>
            <td>Rp {{number_format($i->qty * $i->harga,0,',','.')}}</td>
        </tr>
        @endforeach
        <hr>

    </table>
    <table>
            <th style="width:410px">
            {{-- <p style="font-size:12px; color:red; width:300px; position:absolute">{{$cashback}}</p> --}}
            </th>
            <th style="width:100px;">Total :</th>
            <th style="width:250px;">Rp {{number_format($total_harga,0,',','.')}}</th>
    </table>
    <table>
            <th style="width:330px">
            </th>
            <th style="width:180px;">Potongan Diskon :</th>
            <th style="width:250px;">Rp {{number_format($earn,0,',','.')}}</th>
    </table>
    <hr>
    <table>
            <th style="width:365px">
            </th>
            <th style="width:150px;">Total Harga :</th>
            <th style="width:250px;color:red">Rp {{number_format($final_total,0,',','.')}}</th>
    </table>

    <table style="margin-top: 70px;padding-left:250px">
        <tr>
            <th style="width:250px">Diterima oleh <br><br><br><br><br><br> (.......................)</th>
            <th style="width:250px">Disetujui oleh <br><br><br><br><br><br> (.......................)</th>
        </tr>
    </table>


  </body>
</html>
