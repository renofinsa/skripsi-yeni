<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Data Pelanggan</title>
  </head>
  <body>
  <img src="img/logo_indomasjaya.png" alt="">
  <hr>
  <h2 style="text-align:center">Data Pelanggan</h2>
  <p>
      {{-- No. Invoice : {{$data->id}}<br> --}}
  </p>
  <hr>
  <table>
        <tr>
            <th style="width:50px;padding:5px">No</th>
            <th style="width:100px;padding:5px">Nama</th>
            <th style="width:120px;padding:5px">Telepon</th>
            <th style="width:120px;padding:5px">Email</th>
            <th style="width:200px;padding:5px">Alamat</th>
        </tr>
        <?php $no = 0 ?>
        @foreach ($data as $i)
        <?php $no++ ?>
        <tr>
            <td style="padding:5px">{{$no}})</td>
            <td style="padding:5px">{{$i->nama}}</td>
            <td style="padding:5px">{{$i->telepon}}</td>
            <td style="padding:5px">{{$i->email}}</td>
            <td style="padding:5px">{{$i->alamat}}</td>
        </tr>

        @endforeach
        <hr>
    </table>

  </body>
</html>
