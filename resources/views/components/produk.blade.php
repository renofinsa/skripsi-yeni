<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Data Produk</title>
  </head>
  <body>
  <img src="img/logo_indomasjaya.png" alt="">
  <hr>
  <h2 style="text-align:center">Data Produk</h2>
  <p>
      {{-- No. Invoice : {{$data->id}}<br> --}}
  </p>
  <hr>
  <table>
        <tr>
            <th style="width:50px;padding:5px">No</th>
            <th style="width:150px;padding:5px">Id Produk</th>
            <th style="width:320px;padding:5px">Nama Produk</th>
            <th style="width:220px;padding:5px">Harga</th>
        </tr>
        <?php $no = 0 ?>
        @foreach ($data as $i)
        <?php $no++ ?>
        <tr>
            <td style="padding:5px">{{$no}})</td>
            <td style="padding:5px">{{$i->idproduk}}</td>
            <td style="padding:5px">{{$i->nama_produk}}</td>
            <td style="padding:5px">{{$i->harga}}</td>
        </tr>

        @endforeach
        <hr>
    </table>

  </body>
</html>
