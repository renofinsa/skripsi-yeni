<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Invoice!</title>
  </head>
  <body>
  <img src="img/logo_indomasjaya.png" alt="">
  <hr>
  <h2 style="text-align:center">Delivery Order</h2>
  <p>
      No. DO : #{{$invoice->invoice_number}}<br>
      Tanggal Pembuatan : {{date('d M Y', strtotime($invoice->tanggal_pembuatan))}} <br>
      Dikirim ke : <span style="text-transform: uppercase">{{$invoice->pelanggan->nama}}</span> <br>
      Alamat : {{$invoice->pelanggan->alamat}}
  </p>
  <hr>
  <table>
        <tr>
            <th style="width:50px">No</th>
            <th style="width:500px">Nama Produk</th>
            <th style="width:200px">Qty</th>
        </tr>
        <?php $no = 0 ?>
        @foreach ($detail as $i)
        <?php $no++ ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$i->produk->nama_produk}}</td>
            <td>{{number_format($i->qty,0,',','.')}} Pck</td>
        </tr>
        @endforeach
        <hr>
    </table>
    <h4 style="float:right; margin-right:100px; padding-top: 10px">Total Qty : {{number_format($total_harga,0,',','.')}} Pck</h4>

    <table style="margin-top: 70px">
        <tr>
            <th style="width:250px">Penerima</th>
            <th style="width:250px">Pengirim</th>
            <th style="width:150px">Purchasing</th>
        </tr>
    </table>


  </body>
</html>
