@if(\Session::has('alert'))
            <div class="alert alert-success" role="alert" style="text-align:right;">
                    <p>{{\Session::get('alert')}}</p>
                </div>
                @endif
