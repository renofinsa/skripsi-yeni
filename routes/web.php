<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('login');
});
Auth::routes();
Route::middleware('guest')->group(function () {


});

Route::group(['middleware' => ['authdirector'],'prefix' => 'director'],function (){
    Route::get('/', 'DirectorController@index')->name('director');
     // invoice
    Route::get('/invoice','DirectorController@invoice')->name('director-invoice');
    Route::post('/laporan-penjualan','DirectorController@invoicesemua')->name('director-invoicesemua');
    Route::get('/piutang','DirectorController@piutang')->name('director-piutang');
    Route::get('/invoice/print={id}','DirectorController@print')->name('director-invoice-print');
    // Route::get('/invoice/print-all','AccountingController@print-all')->name('sales-invoice-print-all');
    // // debit / hutang
    Route::post('/laporan-piutang','DirectorController@debit')->name('director-debit-download');
    Route::get('/debit','DirectorController@debit_piutang')->name('director-debit');
    // Route::get('/debit/update={id}','AccountingController@debit-update')->name('sales-debit-update');
    // Route::get('/invoice/print={id}','AccountingController@print')->name('sales-invoice-print');
    // // payment
    Route::get('/payment','DirectorController@payment')->name('director-payment');
    Route::patch('/payment/{id}','DirectorController@dopayment')->name('director-payment-do');
    Route::get('/nota/print={id}','DirectorController@nota')->name('director-nota');
    Route::post('/nota/laporan-pelunasan','DirectorController@pelunasan')->name('director-pelunasan');
});

Route::group(['middleware' => ['authaccounting'],'prefix' => 'accounting'],function (){
    Route::get('/','AccountingController@index')->name('accounting');
    // invoice
    Route::get('/invoice','AccountingController@invoice')->name('accounting-invoice');
    Route::post('/laporan-penjualan','AccountingController@invoicesemua')->name('accounting-invoicesemua');
    Route::get('/piutang','AccountingController@piutang')->name('accounting-piutang');
    Route::get('/invoice/print={id}','AccountingController@print')->name('accounting-invoice-print');
    // Route::get('/invoice/print-all','AccountingController@print-all')->name('sales-invoice-print-all');
    // // debit / hutang
    Route::post('/laporan-piutang','AccountingController@debit')->name('accounting-debit-download');
    Route::get('/debit','AccountingController@debit_piutang')->name('accounting-debit');
    // Route::get('/debit/update={id}','AccountingController@debit-update')->name('sales-debit-update');
    // Route::get('/invoice/print={id}','AccountingController@print')->name('sales-invoice-print');
    // // payment
    Route::get('/payment','AccountingController@payment')->name('accounting-payment');
    Route::patch('/payment/{id}','AccountingController@dopayment')->name('accounting-payment-do');
    Route::get('/nota/print={id}','AccountingController@nota')->name('accounting-nota');
    Route::post('/nota/laporan-pelunasan','AccountingController@pelunasan')->name('accounting-pelunasan');

});

Route::group(['middleware' => ['authsales'],'prefix' => 'sales'],function (){

    // delivery order / do
    Route::get('/','SalesController@index')->name('sales');
    Route::get('/do','PenjualanController@index')->name('sales-do');
    Route::get('/do/add','PenjualanController@create')->name('sales-do-create');
    Route::get('/do/add={id}','PenjualanController@addproduct')->name('sales-do-addproduct');
    Route::post('/do/add/product','PenjualanController@addproductstore')->name('sales-do-addproduct-store');
    Route::delete('/do/add/product={id}','PenjualanController@addproductdelete')->name('sales-do-addproduct-delete');
    // delivery order print
    Route::get('/do/print={id}','PenjualanController@getdo')->name('sales-do-print');
    // pengiriman
    Route::get('/delivery','PengirimanController@index')->name('sales-delivery');
    Route::patch('/delivery/update={id}','PengirimanController@update')->name('sales-delivery-update');

    Route::post('/do/store','PenjualanController@store')->name('sales-do-store');
    // produk
    Route::get('/produk','ProdukController@index')->name('sales-produk');
    Route::get('/produk/add','ProdukController@create')->name('sales-produk-create');
    Route::post('/produk/store','ProdukController@store')->name('sales-produk-store');
    Route::get('/produk/update={id}','ProdukController@edit')->name('sales-produk-update');
    Route::patch('/produk/update={id}','ProdukController@update')->name('sales-produk-update');
    Route::get('/produk/download','ProdukController@download')->name('sales-produk-download');
    // pelanggan
    Route::get('/pelanggan','CustomerController@index')->name('sales-pelanggan');
    Route::get('/pelanggan/add','CustomerController@create')->name('sales-pelanggan-create');
    Route::post('/pelanggan/store','CustomerController@store')->name('sales-pelanggan-store');
    Route::get('/pelanggan/update={id}','CustomerController@edit')->name('sales-pelanggan-update');
    Route::patch('/pelanggan/update={id}','CustomerController@update')->name('sales-pelanggan-update');
    Route::get('/pelanggan/download','CustomerController@download')->name('sales-pelanggan-download');
});
