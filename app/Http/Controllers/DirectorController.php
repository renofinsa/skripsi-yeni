<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penjualan;
use App\Produk;
use App\DetailPenjualan;
use App\Customer;
use PDF;
use DB;
class DirectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        // $data = Auth::user()->email;
        // dd($data);
        return view('director.index');
    }

    public function invoice()
    {
        $customer = Customer::orderBy('nama','desc')->get();
        $invoice = Penjualan::where('status','!=',1)->orderBy('updated_at','desc')->get();
        return view('director.invoice',compact('invoice','customer'));
    }

    public function payment()
    {
        $customer = Customer::orderBy('nama','desc')->get();
        $invoice = Penjualan::where('status',3)->orderBy('tanggal_pelunasan','desc')->get();
        return view('director.payment',compact('invoice','customer'));
    }

    public function dopayment(Request $request, $id)
    {
        $invoice = Penjualan::find($id);
        $invoice->status = 3;
        $invoice->tanggal_pelunasan = $request->get('tanggal_pelunasan');
        $invoice->save();
        return redirect()->back()->with('alert','Invoice telah terkonfirmasi melakukan pembayaran, cek pada menu pembayaran untuk melihat detail.');
    }

    public function invoicesemua(Request $request)
    {
        $customer_id = $request->get('customer_id');
        $from = $request->get('from');
        $to = $request->get('to');
        $db_id = '';
        if($customer_id == 0){
            $db_id = '';
        }else{
            $db_id = 'customer_id = '.$customer_id.' AND';

        }

        $query = 'SELECT a.status, c.nama ,a.invoice_number as invoice,a.tanggal_pembuatan, a.tanggal_jatuh_tempo, SUM(b.harga * b.qty) as total_harga FROM penjualans a
        left join detail_penjualans b on a.id = b.penjualans_id
        left join customers c on a.customer_id = c.id
        where '.$db_id.' (a.status = 2 OR a.status = 3) AND (a.tanggal_pembuatan between "'.$from.'" AND "'.$to.'")
        group by a.id
        order by c.nama asc
        ';

        $posts = DB::select($query);
        // (product_id < 4 AND product_name <> 'Banana');
        $total = DB::select('SELECT SUM(b.harga * b.qty) as total_harga FROM penjualans a
            left join detail_penjualans b on a.id = b.penjualans_id
            where '.$db_id.' (a.status = 2 OR a.status = 3) AND (a.tanggal_pembuatan between "'.$from.'" AND "'.$to.'")
        ');

        // return view('components.report',compact('posts','total'));
        $pdf = PDF::loadview('components.penjualan',['posts'=>$posts,'total'=>$total,'from'=>$from,'to'=>$to]);
        return $pdf->download('laporan_penjualan_'.$from.'_'.$to.'_'.rand(10,100).'.pdf');
        // return $pdf->stream();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function debit_piutang(){
        $customer = Customer::orderBy('nama','desc')->get();
        $invoice = Penjualan::where('status',2)->orderBy('updated_at','desc')->get();
        return view('director.piutang',compact('invoice','customer'));
    }

    public function debit(Request $request)
    {
        $customer_id = $request->get('customer_id');
        $from = $request->get('from');
        $to = $request->get('to');
        $db_id = '';
        if($customer_id == 0){
            $db_id = '';
        }else{
            $db_id = 'customer_id = '.$customer_id.' AND';

        }

        $query = '
        SELECT c.nama ,a.invoice_number as invoice,a.tanggal_pembuatan, a.tanggal_jatuh_tempo, SUM(b.harga * b.qty) as total_harga FROM penjualans a
        left join detail_penjualans b on a.id = b.penjualans_id
        left join customers c on a.customer_id = c.id
        where '.$db_id.' a.status = 2 AND (a.tanggal_pembuatan between "'.$from.'" AND "'.$to.'")
        group by a.id
        ';


        $posts = DB::select($query);
        // dd($posts);


        $total = DB::select('SELECT SUM(b.harga * b.qty) as total_harga FROM penjualans a
        left join detail_penjualans b on a.id = b.penjualans_id
        where '.$db_id.' (a.status = 2 ) AND (a.tanggal_pembuatan between "'.$from.'" AND "'.$to.'")

        ');

        $pdf = PDF::loadview('components.piutang',['posts'=>$posts,'total'=>$total,'from'=>$from,'to'=>$to]);
        return $pdf->download('laporan_piutang_'.$from.'_'.$to.'_'.rand(10,100).'.pdf');
        // return $pdf->stream();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print($id)
    {
        $invoice = Penjualan::find($id);
        $product = Produk::orderBy('idproduk','desc')->get();
        $detailproduct = DetailPenjualan::where('penjualans_id',$id)->get();
        $ambil_total = DetailPenjualan::where('penjualans_id',$id)->get();
        $total = 0;
        foreach ($ambil_total as $key) {
            $sub_total = $key['qty'] * $key['harga'];
            $total += $sub_total;
        }

        $pdf = PDF::loadview('components.invoice',['invoice'=>$invoice,'detail'=>$detailproduct,'total_harga'=>$total]);
        // variable = Class::key('nama_file_download',['get_variable_data'=>variable_data])
        return $pdf->stream();
    }

    public function pelunasan(Request $request)
    {
        $customer_id = $request->get('customer_id');
        $from = $request->get('from');
        $to = $request->get('to');
        $db_id = '';
        if($customer_id == 0){
            $db_id = '';
        }else{
            $db_id = 'customer_id = '.$customer_id.' AND';

        }
        $posts = DB::select('SELECT c.nama ,a.invoice_number as invoice,a.tanggal_pembuatan, a.tanggal_jatuh_tempo, SUM(b.harga * b.qty) as total_harga FROM penjualans a
        left join detail_penjualans b on a.id = b.penjualans_id
        left join customers c on a.customer_id = c.id
        where '.$db_id.' a.status = 3 AND (a.tanggal_pembuatan between "'.$from.'" AND "'.$to.'")
        group by a.id
        ');
        $total = DB::select('SELECT SUM(b.harga * b.qty) as total_harga FROM penjualans a
        left join detail_penjualans b on a.id = b.penjualans_id
        where '.$db_id.' a.status = 3 AND (a.tanggal_pembuatan between "'.$from.'" AND "'.$to.'")

        ');

        $pdf = PDF::loadview('components.pelunasan',['posts'=>$posts,'total'=>$total,'from'=>$from,'to'=>$to]);
        // return $pdf->download('laporan_pelunasan_'.$from.'_'.$to.'_'.rand(10,100).'.pdf');
        return $pdf->stream();

    }

    public function nota($id)
    {
        $invoice = Penjualan::find($id);
        $product = Produk::orderBy('idproduk','desc')->get();
        $detailproduct = DetailPenjualan::where('penjualans_id',$id)->get();
        $ambil_total = DetailPenjualan::where('penjualans_id',$id)->get();
        $total = 0;
        foreach ($ambil_total as $key) {
            $sub_total = $key['qty'] * $key['harga'];
            $total += $sub_total;
        }
        $tanggal_pembuatan = date('d M Y', strtotime($invoice->tanggal_pembuatan));
        $tanggal_pelunasan = date('d M Y', strtotime($invoice->tanggal_pelunasan));
        $tanggal_tempo = date('d M Y', strtotime($invoice->tanggal_jatuh_tempo));

        $final_total = 0;
        $cashback = 0;
        $earn = 0;
        if($tanggal_pelunasan <= $tanggal_tempo){
            // dd('kurang dari');
            $final_total = $total - $total * $invoice->diskon / 100;
            $cashback = 'Mendapatkan cashback '.$invoice->diskon.'% karena pembayaran tidak lebih dari tanggal jatuh tempo';
            $earn = $total * $invoice->diskon / 100;
        }else{
            $final_total = $total;
            $cashback = 'Tidak mendapatkan cashback karena melewati tanggal jatuh tempo.';
            $earn = 0;
            // dd('lebih dari');
        }

        $pdf = PDF::loadview('components.nota',['invoice'=>$invoice,'detail'=>$detailproduct,'total_harga'=>$total,'final_total'=>$final_total,'cashback'=>$cashback,'earn'=>$earn]);
        // variable = Class::key('nama_file_download',['get_variable_data'=>variable_data])
        return $pdf->stream();
    }

}
