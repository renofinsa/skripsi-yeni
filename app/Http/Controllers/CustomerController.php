<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use PDF;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Customer::orderBy('created_at','desc')->get();
        return view('sales.pelanggan',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sales.pelanggan_create');
    }

    public function download()
    {
        $data = Customer::orderBy('nama','asc')->get();

        $pdf = PDF::loadview('components.customer',['data'=>$data]);
        return $pdf->stream();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Customer::find($id);
        return view('sales.pelanggan_update',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vData = $request->validate([
            'nama' => ['required', 'string', 'max:100', 'min:3','max:100'],
            'alamat' => ['required', 'string', 'min:10','max:150'],
            'telepon' => ['required', 'string', 'min:10','max:13'],
            'email' => ['required', 'string', 'min:5','max:100'],
        ]);

        $d = new Customer;
        $d->nama = $vData['nama'];
        $d->alamat = $vData['alamat'];
        $d->telepon = $vData['telepon'];
        $d->email = $vData['email'];
        $d->save();
        return redirect('sales/pelanggan')->with('alert','Berhasil diubah.');
    }


    public function update(Request $request, $id)
    {
        $vData = $request->validate([
            'nama' => ['required', 'string', 'max:100', 'min:3','max:100'],
            'alamat' => ['required', 'string', 'min:10','max:150'],
            'telepon' => ['required', 'string', 'min:10','max:13'],
            'email' => ['required', 'string', 'min:5','max:100'],
        ]);

        $d = Customer::find($id);
        $d->nama = $vData['nama'];
        $d->alamat = $vData['alamat'];
        $d->telepon = $vData['telepon'];
        $d->email = $vData['email'];
        $d->save();
        return redirect('sales/pelanggan')->with('alert','Berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }
}
