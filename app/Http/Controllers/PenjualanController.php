<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use PDF;

use App\Penjualan;
use App\Customer;
use App\Produk;
use App\DetailPenjualan;

class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoice = Penjualan::where('status',1)->orderBy('created_at','desc')->get();
        return view('sales.do',compact('invoice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pelanggan = Customer::orderBy('nama','desc')->get();
        return view('sales.do_create',compact('pelanggan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vData = $request->validate([
            'customer_id' => ['required'],
            'diskon' => ['required', 'string', 'min:1','max:2'],
            'tanggal_jatuh_tempo' => ['required'],
        ]);

        // buat invoice berdasarkan tanggal dan nomer yang berurutan
        $new_date = date("ym");
        $invoice = Penjualan::selectRaw('LPAD(CONVERT(COUNT("invoice_number") + 1, char(8)) , 4,"0") as invoice')->first();

        $d = new Penjualan;
        $d->customer_id = $vData['customer_id'];
        $d->invoice_number = $new_date . $invoice->invoice;
        $d->diskon = $vData['diskon'];
        $d->tanggal_jatuh_tempo = $vData['tanggal_jatuh_tempo'];
        $d->tanggal_pembuatan = date("Y-m-d");
        $d->tanggal_pengiriman = null;
        $d->tanggal_pelunasan = null;
        $d->status = 1;
        // dd($d);
        $d->save();
        return redirect()->route('sales-do-addproduct',$d->id);
    }

    public function addproduct($id)
    {
        $invoice = Penjualan::find($id);
        $product = Produk::orderBy('idproduk','desc')->get();
        $detailproduct = DetailPenjualan::where('penjualans_id',$id)->get();
        return view('sales.do_add_product',compact('invoice','product','detailproduct'));
    }

    public function addproductstore(Request $request)
    {
        $id = $request->get('produks_id');
        $id_penjualan = $request->get('penjualans_id');
        $check = DetailPenjualan::where('penjualans_id',$id_penjualan)->where('produks_id',$id)->get();

        if(empty($check[0])){
            // jika tidak tersedia
            $product = Produk::find($id);
            $d = new DetailPenjualan;
            $d->penjualans_id = $request->get('penjualans_id');
            $d->produks_id = $id;
            $d->harga = $product->harga;
            $d->qty = $request->get('qty');
            $d->save();
            return redirect()->back()->with('alert','Produk ditambah');
        }else{
            // jika tersedia
            $old_id = $check[0]->id;
            $get_sum = $check[0]->qty + $request->get('qty');

            $d = DetailPenjualan::find($old_id);
            $d->qty = $get_sum;
            $d->save();
            return redirect()->back()->with('alert','Produk ditambah');
        }
    }

    public function addproductdelete($id)
    {
        $data = DetailPenjualan::find($id);
        $data->delete();
        return redirect()->back()->with('alert','Produk terhapus.');
    }

    public function getdo($id)
    {
        $invoice = Penjualan::find($id);
        $product = Produk::orderBy('idproduk','desc')->get();
        $detailproduct = DetailPenjualan::where('penjualans_id',$id)->get();
        $ambil_total = DetailPenjualan::where('penjualans_id',$id)->get();
        $total = 0;
        foreach ($ambil_total as $key) {
            $total += $key['qty'];
        }
        // dd($total);

        $pdf = PDF::loadview('components.do',['invoice'=>$invoice,'detail'=>$detailproduct,'total_harga'=>$total]);
        // variable = Class::key('nama_file_download',['get_variable_data'=>variable_data])
        return $pdf->stream();

    }

    public function getinvoice($id)
    {
        $invoice = Penjualan::find($id);
        $product = Produk::orderBy('idproduk','desc')->get();
        $detailproduct = DetailPenjualan::where('penjualans_id',$id)->get();
        $ambil_total = DetailPenjualan::where('penjualans_id',$id)->get();
        $total = 0;
        foreach ($ambil_total as $key) {
            $sub_total = $key['qty'] * $key['harga'];
            $total += $sub_total;
        }
        // dd($total);

        $pdf = PDF::loadview('components.do',['invoice'=>$invoice,'detail'=>$detailproduct,'total_harga'=>$total]);
        // variable = Class::key('nama_file_download',['get_variable_data'=>variable_data])
        return $pdf->stream();

    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function show(Penjualan $penjualan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function edit(Penjualan $penjualan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Penjualan $penjualan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penjualan $penjualan)
    {
        //
    }
}
