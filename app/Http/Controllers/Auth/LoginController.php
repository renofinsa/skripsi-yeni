<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    protected function redirectTo ()
    {
        try {
            if (Auth::user()->role == 1){
                // dd(Auth::user()->email);
                return '/director';
            } else if (Auth::user()->role == 2) {
                // dd(Auth::user()->role);
                return '/accounting';
            } else if (Auth::user()->role == 3) {
                // dd(Auth::user()->role);
                return '/sales';
            }else{
                return '/logout';
            }
        } catch (\Throwable $th) {
            return '/logout';
            //throw $th;
        }
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
        $request->session()->flush();
        Auth::logout();
        return redirect('/');
    }
}
