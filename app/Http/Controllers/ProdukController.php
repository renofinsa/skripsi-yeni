<?php

namespace App\Http\Controllers;

use App\Produk;
use Illuminate\Http\Request;
use PDF;
class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Produk::orderBy('created_at','desc')->get();
        return view('sales.produk',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function download()
    {
        $data = Produk::orderBy('id','asc')->get();
        $pdf = PDF::loadview('components.produk',['data'=>$data]);
        return $pdf->stream();
    }

    public function create()
    {
        return view('sales.produk_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vData = $request->validate([
            'idproduk' => ['required', 'string', 'max:100', 'min:4','max:6', 'unique:produks'],
            'nama_produk' => ['required', 'string', 'min:3','max:100'],
            'harga' => ['required', 'string', 'min:3','max:9'],
        ]);

        $d = new Produk;
        $d->idproduk = $vData['idproduk'];
        $d->nama_produk = $vData['nama_produk'];
        $d->harga = $vData['harga'];
        $d->save();
        return redirect('sales/produk')->with('alert','Berhasil ditambah.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Produk::find($id);
        return view('sales.produk_update',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vData = $request->validate([
            'id_produk' => ['required', 'string', 'max:100', 'min:4','max:6'],
            'nama_produk' => ['required', 'string', 'min:3','max:100'],
            'harga' => ['required', 'string', 'min:3','max:9'],
        ]);

        $d = Produk::find($id);
        $d->idproduk = $vData['id_produk'];
        $d->nama_produk = $vData['nama_produk'];
        $d->harga = $vData['harga'];
        $d->save();
        return redirect('sales/produk')->with('alert','Berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produk $produk)
    {
        //
    }
}
