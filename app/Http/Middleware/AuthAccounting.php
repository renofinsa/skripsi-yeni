<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AuthAccounting
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        try {
            if(Auth::guard($guard)->user()->role == 2){
                return $next($request);
            }else{
                return redirect('/accounting');
            }
        } catch (\Throwable $th) {
            return redirect('/logout');
        }
    }
}
