<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AuthSales
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        try {
            if(Auth::guard($guard)->user()->role == 3){
                return $next($request);
            }else{
                return redirect('/sales');
            }
        } catch (\Throwable $th) {
            return redirect('/logout');
        }
    }
}
