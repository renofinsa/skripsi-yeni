<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    public function detail(){
        return $this->hasMany(DetailPenjualan::class,'penjualans_id');
    }
    public function pelanggan(){
        return $this->belongsTo(Customer::class,'customer_id');
    }
}
