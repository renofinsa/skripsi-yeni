<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPenjualan extends Model
{
    public function produk(){
        return $this->belongsTo(Produk::class,'produks_id');
    }

    public function invoice(){
        return $this->belongsTo(Penjualan::class,'penjualans_id');
    }
}
