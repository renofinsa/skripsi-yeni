<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email'=> 'director@indomasjaya.xyz',
            'password'=> bcrypt('admin'),
            'role'=> 1
        ]);
        User::create([
            'email'=> 'accounting@indomasjaya.xyz',
            'password'=> bcrypt('admin'),
            'role'=> 2
        ]);
        User::create([
            'email'=> 'adminsales@indomasjaya.xyz',
            'password'=> bcrypt('admin'),
            'role'=> 3
        ]);
    }
}
